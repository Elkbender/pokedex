package no.elkbender.pokedex

import android.app.Fragment
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import kotlinx.android.synthetic.main.fragment_search.*
import okhttp3.*
import org.jetbrains.anko.db.select
import java.io.IOException




class SearchFragment : Fragment() {
    private val client = OkHttpClient()
    private val baseUrl: String = "http://pokeapi.co/api/v2/"

    private var editText: EditText? = null
    private var mListener: SearchFragment.OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editText = activity.findViewById(R.id.search_pokemon)
        setEditTextActions()
    }

    override fun onResume() {
        super.onResume()
        MainActivity.setTitle(activity, "Search")
    }

    private fun setEditTextActions() {
        val searchButton: ImageButton = activity.findViewById(R.id.search_button)

        searchButton.setOnClickListener({
            doSearch()
        })

        editText?.setOnKeyListener({ _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                when (keyCode) { KeyEvent.KEYCODE_ENTER -> {
                        doSearch()
                    }
                }
            }
            false
        })
    }

    private fun doSearch() {
        val imm: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText?.windowToken, 0)

        search_button.visibility = View.GONE
        search_pokemon.visibility = View.GONE
        searching_progress.visibility = View.VISIBLE

        val searchString: String = editText?.text.toString().toLowerCase().trim()
        context.database.use {
            select("name").whereArgs("name = {pokemonName}", "pokemonName" to searchString)
        }
        run(baseUrl, searchString)
    }

    private fun run(url: String, search: String) {
        val request = Request.Builder()
                .url(url + arguments.getString(SEARCH_TYPE_KEY) + search)
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {}

            override fun onResponse(call: Call, response: Response) {
                activity.runOnUiThread({MainActivity.showFragment(PokemonInfoFragment.newInstance(response.body()?.string()), activity)})

            }
        })
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        val SEARCH_TYPE_KEY = "search_type"
        val SEARCH_TYPE_POKEMON: String = "pokemon/"
        val SEARCH_TYPE_BERRIES: String = "berries/"

        fun newInstance(searchType: String): SearchFragment {
            val fragment = SearchFragment()
            val args = Bundle()

            args.putString(SEARCH_TYPE_KEY, searchType)
            fragment.arguments = args

            return fragment
        }
    }
}