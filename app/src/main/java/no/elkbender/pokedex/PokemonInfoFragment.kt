package no.elkbender.pokedex

import android.app.Fragment
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_pokemon_info.*
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL


class PokemonInfoFragment : Fragment() {
    private var mListener: PokemonInfoFragment.OnFragmentInteractionListener? = null
    private lateinit var jsonData: String
    private lateinit var pokemon: Pokemon

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        jsonData = arguments.getString(POKEMON_INFO_KEY)

        doAsync {
            val sprite: Bitmap = downloadImage(JSONObject(jsonData).getJSONObject("sprites").getString("front_default"))
            pokemon = Pokemon(jsonData, sprite)
        }

        return inflater.inflate(R.layout.fragment_pokemon_info, container, false)
    }

    private fun downloadImage(url: String): Bitmap {
        var image = BitmapFactory.decodeResource(null, R.mipmap.no_image_found)
        try {
            val inputStream = URL(url).openStream()
            image = BitmapFactory.decodeStream(inputStream)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        activity.runOnUiThread({
            pokemon_sprite_loading?.visibility = View.GONE
            pokemon_sprite.run {
                setImageBitmap(image)
                visibility = View.VISIBLE
                invalidate()
            }
        })
        return image
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPokemonInfo()
        MainActivity.setTitle(activity, JSONObject(jsonData).getString("name").capitalize())
    }

    private fun setPokemonInfo() {
        val id: String = JSONObject(jsonData).getString("id")
        val type: String = parseTypes(JSONObject(jsonData).getJSONArray("types"))
        var height: String = JSONObject(jsonData).getString("height")
        var weight: String = JSONObject(jsonData).getString("weight")

        height = StringBuilder(height).insert(height.length - 1, ',').toString() + " m"
        weight = StringBuilder(weight).insert(weight.length - 1, ',').toString() + " kg"

        pokemon_id.text = id.padStart(3, '0')
        pokemon_height.text = height.padStart(5, '0')
        pokemon_weight.text = weight.padStart(6, '0')
        pokemon_type.text = type.capitalize()
    }

    private fun parseTypes(types: JSONArray): String {
        var typeString: String = JSONObject(jsonData).getJSONArray("types").getJSONObject(0).getJSONObject("type").getString("name")

        for(i in 1..(types.length() - 1)) {
            typeString = typeString + ", " + types.getJSONObject(i).getJSONObject("type").getString("name")
        }
        return typeString
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PokemonInfoFragment.OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        val POKEMON_INFO_KEY = "pokemon_info"

        fun newInstance(info: String?): PokemonInfoFragment {
            val fragment = PokemonInfoFragment()
            val args = Bundle()

            args.putString(POKEMON_INFO_KEY, info)
            fragment.arguments = args

            return fragment
        }
    }
}
