package no.elkbender.pokedex

import android.app.Activity
import android.app.Fragment
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import no.elkbender.pokedex.SearchFragment.Companion.SEARCH_TYPE_BERRIES
import no.elkbender.pokedex.SearchFragment.Companion.SEARCH_TYPE_POKEMON

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, PokemonInfoFragment.OnFragmentInteractionListener,
        SearchFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        showFragment(SearchFragment.newInstance(SEARCH_TYPE_POKEMON), this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_pokemon -> {
                showFragment(SearchFragment.newInstance(SEARCH_TYPE_POKEMON), this)
            }
            R.id.nav_berries -> {
                showFragment(SearchFragment.newInstance(SEARCH_TYPE_BERRIES), this)
            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFragmentInteraction(uri: Uri) {
    }

    companion object {
        fun showFragment(fragment: Fragment, activity: Activity) {
            activity.fragmentManager.beginTransaction().replace(R.id.content_main, fragment).addToBackStack(null).commit()
        }

        fun setTitle(activity: Activity, title: String) {
            activity.title = title.capitalize()
        }
    }
}
