package no.elkbender.pokedex

import android.content.Context
import android.graphics.Bitmap
import org.jetbrains.anko.db.insert
import org.json.JSONArray
import org.json.JSONObject

data class Pokemon(private val jsonData: String, private val sprite: Bitmap) {
    var id: String = JSONObject(jsonData).getString("id")
    var name: String = parseTypes(JSONObject(jsonData).getJSONArray("types"))
    var height: String = formatHeight(JSONObject(jsonData).getString("height"))
    var weight: String = formatWeight(JSONObject(jsonData).getString("weight"))

    private fun parseTypes(types: JSONArray): String {
        var typeString: String = JSONObject(jsonData).getJSONArray("types").getJSONObject(0).getJSONObject("type").getString("name")

        for(i in 1..(types.length() - 1)) {
            typeString = typeString + ", " + types.getJSONObject(i).getJSONObject("type").getString("name")
        }
        return typeString
    }

    private fun formatHeight(cm: String): String {
        return StringBuilder(cm).insert(cm.length - 1, ',').toString() + " m"
    }

    private fun formatWeight(dm: String): String {
        return StringBuilder(dm).insert(dm.length - 1, ',').toString() + " kg"
    }

    fun storePokemon(context: Context) {
        val db = DatabaseHelper.getInstance(context).writableDatabase
        db.insert("pokemon",
                "id" to id,
                "name" to name,
                "height" to height,
                "weight" to weight,
                "sprite" to sprite)
    }
}